SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `Academia` ;
CREATE SCHEMA IF NOT EXISTS `Academia` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `Academia` ;

-- -----------------------------------------------------
-- Table `Academia`.`persona`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Academia`.`persona` ;

CREATE  TABLE IF NOT EXISTS `Academia`.`persona` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `tipo_documento` VARCHAR(1) NULL ,
  `num_documento` VARCHAR(10) NULL ,
  `nombre` VARCHAR(60) NULL ,
  `apellido` VARCHAR(60) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Academia`.`escuela`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Academia`.`escuela` ;

CREATE  TABLE IF NOT EXISTS `Academia`.`escuela` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(50) NULL ,
  `persona_id_secretaria` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_escuela_persona_idx` (`persona_id_secretaria` ASC) ,
  CONSTRAINT `fk_escuela_persona`
    FOREIGN KEY (`persona_id_secretaria` )
    REFERENCES `Academia`.`persona` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Academia`.`profesor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Academia`.`profesor` ;

CREATE  TABLE IF NOT EXISTS `Academia`.`profesor` (
  `id_persona` INT NOT NULL ,
  `fecha_ultimo_contrato` DATE NULL ,
  `escuela_id` INT NOT NULL ,
  PRIMARY KEY (`id_persona`) ,
  INDEX `fk_profesor_persona1_idx` (`id_persona` ASC) ,
  INDEX `fk_profesor_escuela1_idx` (`escuela_id` ASC) ,
  CONSTRAINT `fk_profesor_persona1`
    FOREIGN KEY (`id_persona` )
    REFERENCES `Academia`.`persona` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_profesor_escuela1`
    FOREIGN KEY (`escuela_id` )
    REFERENCES `Academia`.`escuela` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `Academia` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
