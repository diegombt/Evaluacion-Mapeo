/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import edu.mapeo.persistencia.conf.HibernateUtil;
import edu.mapeo.persistencia.entity.Escuela;
import edu.mapeo.persistencia.entity.Persona;
import edu.mapeo.persistencia.entity.Profesor;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author AULA
 */
public class Test {

    public static final String ANSI_BLUE = "\u001B[34m";

    /**
     * Agrega una persona a la BD con sus atributos.
     *
     * @param tipoDoc
     * @param numDoc
     * @param nombre
     * @param apellido
     */
    public void agregarPersona(String tipoDoc, String numDoc, String nombre, String apellido) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Persona persona = new Persona(tipoDoc, numDoc, nombre, apellido);
        session.save(persona);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Muestra todas las personas en la BD
     */
    public void mostrarPersonas() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String consulta = "from Persona";
        List listaPersonas = session.createQuery(consulta).list();
        Iterator iterator = listaPersonas.iterator();
        System.out.println(ANSI_BLUE+"Las personas registradas son: ");
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Persona persona = (Persona) object;
            System.out.println("→ " + persona.getId() + " " + persona.getNombre() + " " + persona.getNumDocumento());
        }
        session.close();
    }

    /**
     * Agrega una escuela y la secretaria encargada.
     *
     * @param nombre
     * @param idSecretario
     */
    public void agregarEscuela(String nombre, int idSecretario) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Persona persona = (Persona) session.get(Persona.class, idSecretario);
        Escuela escuela = new Escuela(nombre, persona);
        session.save(escuela);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Muestra todas las escuelas registradas en la BD
     */
    public void consultarEscuelas() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String consulta = "from Escuela";
        List listaEscuelas = session.createQuery(consulta).list();
        Iterator iterator = listaEscuelas.iterator();
        System.out.println(ANSI_BLUE+"Estan registradas las siguientes escuelas: ");
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Escuela escuela = (Escuela) object;
            System.out.println("→ " + escuela.getId() + " " + escuela.getNombre());
        }
        session.close();
    }

    /**
     * Muestra el nombre de la escuela con la persona encargada de la
     * secretaría.
     */
    public void consultarEscSec() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String consulta = "from Escuela";
        List listaEscuelas = session.createQuery(consulta).list();
        Iterator iterator = listaEscuelas.iterator();
        System.out.println(ANSI_BLUE+"Escuelas con su respectivo secretari@: ");
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Escuela escuela = (Escuela) object;
            Persona sec = (Persona) session.get(Persona.class, escuela.getSecretario().getId());
            System.out.println("→ " + escuela.getNombre() + ": " + sec.getNombre() + " - " + sec.getNumDocumento());
        }
        session.close();
    }

    /**
     * Consulta las personas que son secretarios y la escuela a la que
     * pertenecen.
     */
    public void consultarSecretarios() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String consulta = "from Persona";
        List listaPersonas = session.createQuery(consulta).list();
        Iterator iterator = listaPersonas.iterator();
        System.out.println(ANSI_BLUE+"Los secretarios registrados son: ");
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Persona persona = (Persona) object;
            if (persona.getEscuela() != null) {
                Escuela esc = (Escuela) session.get(Escuela.class, persona.getEscuela().getId());
                System.out.println("→ " + persona.getNombre() + " es secretari@ de " + esc.getNombre());
            }
        }
        session.close();
    }

    /**
     * Agrega un nuevo profesor a una escuela.
     *
     * @param idPersona
     * @param idEscuela
     * @param ultimoContrato
     */
    public void agregarProfesor(int idPersona, int idEscuela, Date ultimoContrato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Persona persona = (Persona) session.get(Persona.class, idPersona);
        Escuela escuela = (Escuela) session.get(Escuela.class, idEscuela);
        Profesor profesor = new Profesor(escuela, persona, ultimoContrato);
        session.save(profesor);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Consulta todos los profesores registrados en la BD.
     */
    public void consultarProfesores() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String consulta = "from Profesor";
        List listaProfesores = session.createQuery(consulta).list();
        Iterator iterator = listaProfesores.iterator();
        System.out.println(ANSI_BLUE+"Los profesores registrados son:");
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Profesor profesor = (Profesor) object;
            Persona per = (Persona) session.get(Persona.class, profesor.getIdPersona());
            System.out.println("→ " + per.getNombre() + " " + per.getApellido());
        }
        session.close();
    }

    /**
     * Consulta todos los profesores de la escuela: @idEscuela.
     *
     * @param idEscuela
     */
    public void consultarProfEscuela(int idEscuela) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Escuela escuela = (Escuela) session.get(Escuela.class, idEscuela);
        Iterator iterator = escuela.getProfesors().iterator();
        if (!iterator.hasNext()) {
            System.out.println(ANSI_BLUE+"No hay profesores registrados en la escuela: " + escuela.getNombre());
        } else {
            System.out.println(ANSI_BLUE+"Los profesores de " + escuela.getNombre() + " son: ");
            while (iterator.hasNext()) {
                Object object = iterator.next();
                Profesor profesor = (Profesor) object;
                Persona p = (Persona) session.get(Persona.class, profesor.getIdPersona());
                System.out.println("→ " + p.getNombre() + " " + p.getApellido());
            }
        }
        session.close();
    }

    /**
     * Metodo Main
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Test t = new Test();
//        t.agregarPersona("C", "1049625186", "Diego", "Bernal");
//        t.agregarPersona("C", "1049625187", "Anna", "Bernal");
//        t.agregarPersona("C", "1049625188", "Angela", "Bernal");
//        t.agregarPersona("C", "1049625189", "Diana", "Bernal");
//        t.agregarPersona("C", "1049625190", "Camilo", "Bernal");
//        t.agregarPersona("C", "1049625191", "Henry", "Bernal");
//        t.agregarPersona("C", "1049625192", "Patricia", "Bernal");
//        t.agregarPersona("C", "5555555555", "German", "Amezquita");
        t.mostrarPersonas();
//        t.agregarEscuela("Salesiano", 1);
//        t.agregarEscuela("Presentacion", 6);
//        t.agregarEscuela("UPTC", 7);
        t.consultarEscuelas();
        t.consultarEscSec();
        t.consultarSecretarios();
//        t.agregarProfesor(8, 3, Calendar.getInstance().getTime());
//        t.agregarProfesor(2, 3, Calendar.getInstance().getTime());
        t.consultarProfesores();
        t.consultarProfEscuela(3);
        t.consultarProfEscuela(2);
    }

}
